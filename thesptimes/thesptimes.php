<?php
/*
Plugin Name: The São Paulo Times - Notícias
Plugin URI: http://www.spotdev.com.br
Description: Este é um plugin para exibir em seu blog notícias do The São Paulo Times.
Version: 2.0
Author: SpotDev Ltda
Author URI: http://www.spotdev.com.br
License: GPL2
*/

defined( 'ABSPATH' ) or die( 'Acesso proibido!' );


class Dino extends WP_Widget {

	/**
	 * Registra o widget no wordpress.
	 */
	function __construct() {
		parent::__construct(
			'dino_widget',
			__( 'The São Paulo Times - Noticiário', 'dino_text_domain' ),
			array( 'description' => __(
                'Lista das últimas notícias do The São Paulo Times',
                'dino_text_domain' ), )
		);

        add_action( 'add_meta_boxes', array( $this, 'add_meta_box' ) );
        add_action( 'save_post', array( $this, 'save' ) );
        add_action( 'admin_head', array( $this, 'hide_editor') );
        add_action('the_title', array( $this, 'changeTitle'), 0);
        add_action('template_redirect', array( $this, 'changeArticle'), 0);

	}

    /**
	 * Função para mudar o article da noticia
	 */
    function changeArticle(){
        global $post;
        if(!empty($post)){

            $meta_value = get_post_meta($post->ID, 'dinoticia', true);
            $is_dinoticia = $meta_value == 'sim' && !empty($meta_value) ? true : false;

            if($is_dinoticia){
                if(!empty($_GET['d'])){
                    global $titleGrabbed;
                    $notice_id = $_GET['d'];
                    $dino_path = plugin_dir_path(__FILE__);
                    include($dino_path."controller/geraNoticia.php");
                    $this->setNoticia($output);
                    $titleGrabbed = $data['title'];
                }else{
                    $this->setNoticia('A notícia que você esta procurando não pode ser encontrada!');
                }
            }
        }

    }

    /**
	 * Função para mudar o título da notícia
	 */
    function changeTitle($title){
        global $post;
        if(!empty($post)){

	    if($title != $post->post_title) return $title;

            $meta_value = get_post_meta($post->ID, 'dinoticia', true);
            $is_dinoticia = $meta_value == 'sim' && !empty($meta_value) ? true : false;

            if($is_dinoticia){
                global $titleGrabbed;
                if(!empty($_GET['d']) && !empty($titleGrabbed)){
		            $title = $titleGrabbed;
                }else{
		            $title = "Ops! Tivemos um problema";
                }
            }
        }

	   return $title;

    }

    function setNoticia($html){
        global $post;
        $post->post_content = $html;
    }

    /**
	 * Front-End Evento usado para remover o editor da página
	 */
    function hide_editor() {

        global $pagenow;
        if( !( 'post.php' == $pagenow ) ) return;

        global $post;
        $post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
        if( !isset( $post_id ) ) return;

        if(get_post_meta($post_id, 'dinoticia', true) == 'sim')
            remove_post_type_support('page', 'editor');
    }

    /**
	 * Front-End Adiciona o item de configuração na página
	 *
	 * @param WP_Post $post_type tipo do post
	 */
    public function add_meta_box( $post_type ) {
        $post_types = array('post', 'page');
        if ( in_array( $post_type, $post_types )) {
            add_meta_box(
                'some_meta_box_name'
                ,__( 'The São Paulo Times', 'dino_text_domain' )
                ,array( $this, 'render_meta_box_content' )
                ,$post_type
                ,'advanced'
                ,'high'
            );
        }
	}

	/**
	 * Função chamada quando a configuração é salva.
	 *
	 * @param int $post_id id da página que esta sendo salva.
	 */
	public function save( $post_id ) {

		if ( ! isset( $_POST['myplugin_inner_custom_box_nonce'] ) )
			return $post_id;

		$nonce = $_POST['myplugin_inner_custom_box_nonce'];

		if ( ! wp_verify_nonce( $nonce, 'myplugin_inner_custom_box' ) )
			return $post_id;

		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
			return $post_id;

		if ( 'page' == $_POST['post_type'] ) {

			if ( ! current_user_can( 'edit_page', $post_id ) )
				return $post_id;

		} else {

			if ( ! current_user_can( 'edit_post', $post_id ) )
				return $post_id;
		}

        if(!empty($_POST['dinoticia'])){
            $dinoticia = $_POST['dinoticia'];
            $mydata = sanitize_text_field($dinoticia);
            update_post_meta( $post_id, 'dinoticia', $mydata );
        }

        add_action( 'edit_form_after_title', '_wp_posts_page_notice' );
	}


	/**
	 * Front-End da parte de configuraçoes da page
	 *
	 * @param WP_Post $post O post feito
	 */
	public function render_meta_box_content( $post ) {

		wp_nonce_field( 'myplugin_inner_custom_box', 'myplugin_inner_custom_box_nonce' );
		$value = get_post_meta( $post->ID, 'dinoticia', true );

        ?>
        <label for="dinoticia">
            <?=_e( 'Mostrar a notícia nesta página?', 'dino_text_domain' )?>
        </label>

        &nbsp;&nbsp;&nbsp;
        <input type="radio" name="dinoticia" value="sim" <?= $value == "sim" ? "checked" : "" ?>>Sim
        <input type="radio" name="dinoticia" value="nao" <?= $value == "nao" ? "checked" : "" ?>>Não
        <?php
	}

	/**
	 * O Front-end do dino widget
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args argumentos do widget.
	 * @param array $instance valores salvos no banco de dados.
	 */
	public function widget( $args, $instance ) {
        $dino_path = plugin_dir_path(__FILE__);
        $categoria = ! empty( $instance['categoria'] ) ? $instance['categoria'] : "";
        $page_name = ! empty( $instance['page'] ) ? $instance['page'] : "";
        $comBarrinha = ! empty( $instance['comBarrinha'] ) ? $instance['comBarrinha'] : "";

		echo $args['before_widget'];
        unset($instance['title']);
		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ). $args['after_title'];
		}

        include("$dino_path/view/widget-view.php");
		echo $args['after_widget'];
	}

	/**
	 * Back-end do form do widget.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Valores antigos salvos no database.
	 */
	public function form( $instance ) {
//		$title = ! empty( $instance['title'] ) ? $instance['title'] : __( 'New title', 'dino_text_domain' );

		?>

<!--
		<p>
            <label for="<?php //echo $this->get_field_id( 'title' ); ?>"><?php //_e( 'Title:' ); ?></label>
            <input class="widefat"
               id="<?php //echo $this->get_field_id( 'title' ); ?>"
               name="<?php //echo $this->get_field_name( 'title' ); ?>"
               type="text"
               value="<?php //echo esc_attr( $title ); ?>">
		</p>
-->

		<?php
        $categoria = ! empty( $instance['categoria'] ) ? $instance['categoria'] : __( 'Categoria', 'dino_text_domain' );
        $lista_categorias = array('Geral'=>"",
          'Brasil'=>"categoria/brasil/",
          'Internacional'=>"categoria/mundo/",
          'Negocios'=>"categoria/negocios/",
          'Tendências'=>"categoria/newsweektrends/",
          'Tecnologia'=>"categoria/tecnologia/",
          'Saúde'=>"categoria/saude/");
		?>

		<p>
            <label for="<?php echo $this->get_field_id( 'categoria' ); ?>"><?php _e( 'Categoria:' ); ?></label>
            <select class="widefat"
                   id="<?php echo $this->get_field_id( 'categoria' ); ?>"
                   name="<?php echo $this->get_field_name( 'categoria' ); ?>"
                   type="text">
                <?php foreach($lista_categorias as $key => $nome): ?>
                    <?php if($categoria != $nome): ?>
                        <option value="<?=$nome?>"><?=$key?></option>
                    <?php else: ?>
                        <option value="<?=$nome?>" selected="selected"><?=$key?></option>
                    <?php endif; ?>
                <?php endforeach; ?>
            </select>
		</p>
		<?php

        $page = ! empty( $instance['page'] ) ? $instance['page'] : __( 'Página', 'dino_text_domain' );
        $args = array(
            'meta_key' => 'dinoticia',
            'meta_value' => 'sim',
            'post_type' => 'page',
        );
        $posts = get_posts($args);

        foreach($posts as $pval){

            $title = $pval->post_title == false ? 'Sem título' : $pval->post_title;
            $lista_paginas[$title] = get_permalink($pval);

        }

        ?>
		<p>
            <label for="<?php echo $this->get_field_id( 'page' ); ?>"><?php _e( 'Página:' ); ?></label>
            <select class="widefat"
                   id="<?php echo $this->get_field_id( 'page' ); ?>"
                   name="<?php echo $this->get_field_name( 'page' ); ?>"
                   type="text">
                <?php foreach($lista_paginas as $key => $nome): ?>
                    <?php if($page != $nome): ?>
                        <option value="<?=$nome?>"><?=$key?></option>
                    <?php else: ?>
                        <option value="<?=$nome?>" selected="selected"><?=$key?></option>
                    <?php endif; ?>
                <?php endforeach; ?>
            </select>
		</p>
		<?php

        $value = ! empty( $instance['comBarrinha'] ) ? $instance['comBarrinha'] : __( 'Com ScrollBar?', 'dino_text_domain' );
        ?>

        <label for="<?php echo $this->get_field_id( 'comBarrinha' ); ?>"><?php _e( 'Com ScrollBar?:' ); ?></label> <br>
        <input type="radio"
               id="<?php echo $this->get_field_id( 'comBarrinha' ); ?>"
               name="<?php echo $this->get_field_name( 'comBarrinha' ); ?>"
               value="sim" <?= $value == "sim" ? "checked" : "" ?>>Sim
        <input type="radio"
               id="<?php echo $this->get_field_id( 'comBarrinha' ); ?>"
               name="<?php echo $this->get_field_name( 'comBarrinha' ); ?>"
               value="nao" <?= $value == "nao" ? "checked" : "" ?>>Não<br>

        <?php
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		//$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        $instance['categoria'] = ( ! empty( $new_instance['categoria'] ) ) ? strip_tags( $new_instance['categoria'] ) : '';
        $instance['page'] = ( ! empty( $new_instance['page'] ) ) ? strip_tags( $new_instance['page'] ) : '';
        $instance['comBarrinha'] = ( ! empty( $new_instance['comBarrinha'] ) ) ? strip_tags( $new_instance['comBarrinha'] ) : '';

		return $instance;
	}

}

// Registra o widget
add_action('widgets_init',create_function('', 'return register_widget("Dino");'));

?>
