<?php

defined( 'ABSPATH' ) or die( 'Acesso proibido!' );

$output = @file_get_contents('http://saopaulotimes.com.br/sp/'.$categoria.'feed/');
if($output == true){
    $xml = simplexml_load_string($output);

    for ($i=0; $i < count($xml->channel[0]->item); $i++) {
        $doc = new DOMDocument();
        libxml_use_internal_errors(true);
        $doc->loadHTML($xml->channel[0]->item[$i]->children("content", true)->__toString());
        libxml_clear_errors();
        $a = simplexml_import_dom($doc);
        $images = $a->xpath('//img');

        foreach ($images as $img) {
            if (isset($img["src"]))
                $images =  $img['src'];
            break;
        }
        $id = str_replace('http://saopaulotimes.r7.com/sp/?p=', '' ,$xml->channel[0]->item[$i]->guid[0]->__toString());
        $date = date_create($xml->channel[0]->item[$i]->pubDate[0]->__toString());
        $dataLink['date'] = date_format($date, "d/m/Y");
        $dataLink['id'] = $id;
        $dataLink['page_name'] = $page_name;
        $dataLink['image'] = $images;
        $dataLink['title'] = $xml->channel[0]->item[$i]->title[0]->__toString();
        $data[] = $dataLink;
    }

    $categoria = !empty($categoria) ? '&c='.str_replace(array("categoria/","/"), "", $categoria) : '';
    $comBarrinha = empty($comBarrinha) ? "sim" : $comBarrinha;
    $comBarrinha = $comBarrinha == "sim" ? true : false;
}

?>
