<?php
//defined( 'ABSPATH' ) or die( 'Acesso proibido!' );

$categoria = empty($_GET['c']) ? "" : $_GET['c'];
$noticeId = $_GET['d'];

if($categoria != ""){

    $categoria="categoria/".$categoria."/";
    echo $categoria;

}

if(!empty($noticeId)){

    $output = @file_get_contents('http://saopaulotimes.com.br/sp/'.$categoria.'feed/');
    if($output == false){
        $data['title'] = "Ops! Ocorreu um erro";
        $output = "Lamentamos mas esta notícia é inválida...";
        return;
    }

    $xml = simplexml_load_string($output);
    if(count($xml->channel[0]->item) == 0){
        $data['title'] = "Ops! Ocorreu um erro";
        $output = "Lamentamos mas esta notícia é inválida...";
        return;
    }

    for ($i=0; $i <count($xml->channel[0]->item); $i++) {
        if($xml->channel[0]->item[$i]->guid[0]->__toString() == ('http://saopaulotimes.r7.com/sp/?p='.$noticeId)){
            $data['title'] = $xml->channel[0]->item[$i]->title[0]->__toString();
            $date = $xml->channel[0]->item[$i]->pubDate[0]->__toString();
            $data['date'] = date_i18n(get_option( 'date_format' ), strtotime( $date ));
            $data['article'] = $xml->channel[0]->item[$i]->children("content", true)->__toString();
            break;
        }
    }

    if(empty($data)){

        $output = @file_get_contents('http://saopaulotimes.r7.com/sp/?p='.$noticeId);
        $doc = new DOMDocument();
        libxml_use_internal_errors(true);

        // Repara o charset bugado do utf-8
        $doc->loadHTML('<?xml encoding="UTF-8">' . $output);
        foreach ($doc->childNodes as $item)
        if ($item->nodeType == XML_PI_NODE)
            $doc->removeChild($item);
        $doc->encoding = 'UTF-8';
        libxml_clear_errors();


        // remove flares de redes sociais do sp

        $xpath = new DomXPath($doc);
        $classname='flare-horizontal';
        $xpath_results = $xpath->query("//div[contains(@class, '$classname')]");

        for ($i=0; $i < $xpath_results->length ; $i++) {
            if($div = $xpath_results->item($i)){

                $div->parentNode->removeChild($div);

            }
        }

        //pega titulo

        $xpath = new DomXPath($doc);
        $classname="post-title";
        $xpath_results = $xpath->query("//*[contains(@class, '$classname')]");

        $data['title'] = $doc->saveHTML($xpath_results->item(0));

        //pega date
        $xpath = new DomXPath($doc);
        $classname="post-date";
        $xpath_results = $xpath->query("//*[contains(@class, '$classname')]");

        $data['date'] = $doc->saveHTML($xpath_results->item(0));


        //pega só o article
        $a = $doc->getElementsByTagName('article');

        // pega o post content já sem os flags de rede sociais
        $xpath = new DomXPath($doc);
        $classname="post-content";
        $xpath_results = $xpath->query("//*[contains(@class, '$classname')]");

        $data['article'] = $doc->saveHTML($xpath_results->item(0));


        if(empty($data))
        {
            $data['title'] = "Ops! Ocorreu um erro";
            $output = "Lamentamos mas esta notícia é inválida...";
            return;
        }
    }

    ob_start();
    include("$dino_path/view/noticia-view.php");
    $output = ob_get_clean();
}


?>


