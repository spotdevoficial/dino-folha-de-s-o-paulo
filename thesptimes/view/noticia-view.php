<?php defined( 'ABSPATH' ) or die( 'Acesso proibido!' ) ?>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<style type="text/css">
	@import url(https://fonts.googleapis.com/css?family=Roboto+Slab);
	h1{
		font-family: 'Roboto Slab', serif;
	}

	.post-title{
		font-weight: 400;
		font-size: 32px;
	}
	.date-post::before{
		font-family: 'FontAwesome';
		margin-right: 4px;

		content: "\f073";
	}
	.date-post{
		-webkit-font-smoothing: antialiased;
		border-right-color: rgb(240, 240, 240);
		border-right-style: solid;
		border-right-width: 1px;
		box-sizing: border-box;
		color: rgb(153, 153, 153);
		display: block;
		float: left;
		font-family: Arial, sans-serif;
		font-size: 14px;
		height: 33px;
		line-height: 21px;
		padding-bottom: 6px;
		padding-left: 14px;
		padding-right: 14px;
		padding-top: 6px;

	}
	.metaDate{
		-webkit-font-smoothing: antialiased;
		border-bottom-color: rgb(240, 240, 240);
		border-bottom-style: solid;
		border-bottom-width: 1px;
		border-top-color: rgb(240, 240, 240);
		border-top-style: solid;
		border-top-width: 1px;
		box-sizing: border-box;
		color: rgb(153, 153, 153);
		display: block;
		font-family: Arial, sans-serif;
		font-size: 14px;
		height: 35px;
		line-height: 21px;
		margin-bottom: 20px;
		overflow-x: hidden;
		overflow-y: hidden;
		text-decoration: none;

	}

	img{
		float: left;
		overflow: auto;
		padding: 10px;
	}
</style>
<section>
	<div class="metaDate"><span class="date-post"><?= $data['date'] ?> - Via The São Paulo Times</span></div>
	<article><?= $data['article'] ?></article>
	<br>
</section>
