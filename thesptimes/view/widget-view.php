<?php defined( 'ABSPATH' ) or die( 'Acesso proibido!' ) ?>
<?php include ($dino_path.'/controller/geraLink.php'); ?>

<style type="text/css">

    .logo-widget {
        margin-bottom: 20px;
	    width: 250px;
	    margin-bottom: 20px;
  	    margin-top: 20px;
    }

    .line-separator-wid {

        background-color: gray;
	    margin-top: 10px;

    }

    .dino-title {

        margin-top: 10px;
	    color: black;
  	    font-weight: bold;
  	    font-size: 17px;
	    font-family: 'Roboto Slab', serif

    }

    .dino-bar {

	    padding: 15px;
        border-style: solid;
  	    border-width: 1px;
  	    border-color: rgb(224, 224, 224);
	    background-color: white;

     }

     .logo-content {

	    text-align: center;
	    background-color: rgb(249, 249, 249);
  	    border-style: solid;
  	    border-width: 1px;
  	    border-color: rgb(224, 224, 224);
  	    border-top-left-radius: 4px;
  	    border-top-right-radius: 4px;

     }

    .dino-time {

        font-size: 14px;
        font-family: 'Arial', sans-serif;
        text-transform: uppercase;
        color: #999;

    }

     .dino-widget {

     	-webkit-box-shadow: 1px -1px 5px 0px rgb(221, 221, 221);
	    -moz-box-shadow: 1px -1px 5px 0px rgba(209,209,209,1);
	    box-shadow: 1px -1px 5px 0px rgba(209,209,209,1);

     }

    <?php if($comBarrinha): ?>
        .dino-bar {
            height: 800px;
            overflow-y: scroll;
        }
    <?php endif; ?>

</style>

<div class="dino-widget">
	<div class="logo-content">
		<img class="logo-widget" src="<?= plugins_url( 'images/logo_sp.png', dirname(__FILE__)) ?>"/>
	</div>
	<?php if($output == true): ?>
        <div class="dino-bar">
            <?php foreach($data as $noticia): ?>
                <span class="dino-time"><?=$noticia['date']?></span>
                <a href='<?=$noticia['page_name']?>?d=<?=$noticia['id']?><?=$categoria?>'>
                    <img class='noticeImg' src='<?=$noticia['image']?>'>
                    <p class="dino-title"><?= $noticia['title'] ?></p>
                </a>
                <hr class="line-separator-wid">

            <?php endforeach; ?>
        </div>
	<?php else: ?>
    		<h2>Erro ao tentar se conectar com a Folha de São Paulo</h2>
	<?php endif; ?>
</div>
