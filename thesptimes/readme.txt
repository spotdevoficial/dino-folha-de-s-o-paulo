=== The São Paulo Times - Notícias ===
Contributors: SpotDev Ltda
Donate link: http://www.spotdev.com.br/
Requires at least: 3.0.1
Tested up to: 3.4
Stable tag: 4.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Este é um plugin para exibir em seu blog notícias do The São Paulo Times.

== Description ==

Com o Dino você poderá facilmente criar uma widget de tópicos de nóticias automatico pegando da The São Paulo Times um RSS. As notícias serão automaticamente atualizadas e as notícias em tela cheia da mesma também. 

== Installation ==

1. Faça upload do plugin normalmente
1. Ative o plugin 
1. Crie uma página e marque ela a opção de transformar ela em página de notícias para "Sim"

== Frequently Asked Questions ==

= Como faço para alterar o layout do plugin? =

1 - Para algumas alterações você precisará ir na pasta do plugin, dino-plugin/view/ e lá terá os arquivos para serem editados. 
2 - Para outras o plugin pega o estilo do próprio tema do wordpress.

= Esta dando erro na conexão com a The São Paulo Times Por quê? =

A folha pode ter bloquiado a conexão com seu site temporariamente, ou ela esta offline.

== Changelog ==

= 1.0 =
* Versão inicial do plugin

= 1.5 =
* Mudado nome do plugin
* Reparado margen da página de notícias
* Adicionado nova imagem ao widget
* Adicionado data no widget
* Mudado a aparência do widget
* Removido fonte no final da página

= 1.8 =
* Reparado título
* Reparado descrição do sistema
* Reparado descrições do plugin
* Reparado descrições do plugin

= 2.0 =
* Adicionado opção que permite verificar qualquer noticia a qualquer hora
* Reparado Charset


`<?php code(); // goes in backticks ?>`
